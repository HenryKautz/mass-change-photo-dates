#!/bin/bash -e 

# 10 July 2023 - rewrote to handle arguments in any order and fix explicit time zone
# 16 Jan 2020 -
#    Set CreationDate which is the date field used for video clips.
#    Added -summer and -zone options.
#    Added boulder to city list.
#  Currently not setting the following Quicktime tags which are only applicable to videos:
#     Media Create Date, Media Modify Date, Track Create Date, Track Modify Date, Content Create Date
#  Currently not setting the GPS tags which are only applicable to images:
#     GPSTimeStamp, GPSDateStamp
#  20 May 2022 - fixed octal problem
#  Time zone for mov files not being set correctly

if (( $# == 0 )); then
	echo 'Use: mass-change-photo-dates.sh {-summer} {-city CITY} {-zone +/-N} {-date YYYY-MM-DD} DIRECTORY { DIRECTORY ... }
Extract the date from the DIRECTORY name, which should begin with YYYY-MM-DD, YYYY-MM, or YYYY.  
Change the dates of the image files in the directory in alphabetical order to have dates in increasing 
order starting from noon on the extracted starting date. Dates increase by 60 seconds for each file.

Option -summer indicates daylight savings time is in effect.

Option -city specifies a city used to set the GPS location and time zone.  The GPS location is only set for 
	files that do not have a GPS tag.  Default timezone is Eastern.

Option -zone N specifies the number of hours GMT offset as a positive or negative integer.

Option -date YYYY-MM-DD specifies a date which overrides any directory names.

Option -verbose prints information for each file changed.

If the directories contain subdirectories, then the subdirectories are processed as well if either the 
	subdirectory name is in date format or the -date option is set.
'
	exit 1
fi

(( STARTTIME = 12 ))   # hour used for first photo in directory
(( INCREMENT = 60 ))  # increase in seconds between photos, maximum value 60
(( SUMMER=0 ))
(( TIMEZONEBASE = -5 ))
(( VERBOSE = 0 ))
DATESTRING=''

while [[ "$1" == -* ]]; do
	if [[ "$1" == '-summer' ]]; then
    	(( SUMMER = 1 ))
    	(( TIMEZONEBASE = TIMEZONEBASE + 1 ))
    	shift
	elif [[ "$1" == '-verbose' ]]; then
		(( VERBOSE = 1 ))
		shift
	elif [[ "$1" == '-city' ]]; then
    	shift
    	# convert city name to GPS latitude and longitude
	    case "$1" in
		london )
		    # The N and W are ignored, they actually come from the Ref tags
		    GPSLatitude='51 deg 30'"'"' 35.51" N'
	    	GPSLongitude='0 degree 7'"'"' 5.13" W'
	    	(( TIMEZONEBASE = 0 ))
			;;
		rochester )
	   		GPSLatitude='43 deg 9'"'"' 39.71" N'
		    GPSLongitude='77 deg 36'"'"' 39.33" W'
			(( TIMEZONEBASE = -5 ))
		    ;;
		seattle )
		    GPSLatitude='47 deg 36'"'"' 28.85" N'
	    	GPSLongitude='122 deg 20'"'"' 6.60" W'
			(( TIMEZONEBASE = - 8 ))
		    ;;
		boulder )
	    	GPSLatitude='40 deg 1'"'"' 3.45" N'
		    GPSLongitude='105 deg 16'"'"' 13.98" W'
	    	(( TIMEZONEBASE = -7 ))
		    ;;
		* )
		    echo "Unknown city"
	    	exit 1
			;;
	    esac
		shift
	elif [[ "$1" == '-zone' ]]; then
    	(( TIMEZONEBASE = $1 ))
    	shift
	elif [[ "$1" == '-date' ]]; then
		shift
		DATESTRING="$1"
		if [[ ! ${DATESTRING} =~ ^[12][019][0-9][0-9]-[01][0-9]-[0-9][0-9] ]]; then
			echo "Bad date {$DATESTRING}"
			exit 1
		fi
		shift
	fi

done

if (( $SUMMER )); then
	(( TIMEZONEBASE = TIMEZONEBASE + 1 ))
fi
TIMEZONE=$(printf "%+03d" $TIMEZONEBASE):00
ARGS=( "$@" )

while (( ${#ARGS[@]} > 0)); do

    FULLDIR="${ARGS[0]}"
	ARGS=("${ARGS[@]:1}")
    DIR=`basename "${FULLDIR}"`

	if 	[[ -n "${DATESTRING}" ]]; then
		DIR="${DATESTRING}"
	fi
    if [[ ! ${DIR} =~ ^[12][019][0-9][0-9] ]]; then
		echo "Skipping directory ${FULLDIR}"
		continue
	fi
	echo "Processing directory ${FULLDIR}"

	# Extract date from directory name, setting
	# variables YEAR MONTH DAY with proper format
	(( YEAR= 10#${DIR:0:4} ))
	if [[ ${DIR} =~ ^[12][019][0-9][0-9]-[01][0-9] ]]; then
		(( MONTH = 10#${DIR:5:2} ))
	else
		(( MONTH = 1 ))
	fi
	if [[ ${DIR} =~ ^[12][019][0-9][0-9]-[01][0-9]-[0-9][0-9] ]]; then
		(( DAY = 10#${DIR:8:2} ))
	else
		(( DAY = 1 ))
	fi
	
	FYEAR=`printf "%04d" ${YEAR}`
	FMONTH=`printf "%02d" ${MONTH}`
	FDAY=`printf "%02d" ${DAY}`
	echo "Setting files in ${FULLDIR} to date ${FYEAR}-${FMONTH}-${FDAY}"

    pushd "${FULLDIR}" >/dev/null

    (( HOUR = ${STARTTIME} ))
    (( MIN = 0 ))
    (( SEC = 0 ))

	(( ORGINALS = 0 ))

	for f in * ; do

		if [[ -d "$f" ]]; then
			newarg="${FULLDIR}/${f}"
			ARGS+=("${newarg}")
			continue
		fi


		FYEAR=`printf "%04d" ${YEAR}`
		FMONTH=`printf "%02d" ${MONTH}`
		FDAY=`printf "%02d" ${DAY}`
		FHOUR=`printf "%02d" ${HOUR}`
		FMIN=`printf "%02d" ${MIN}`
		FSEC=`printf "%02d" ${SEC}`


		if (( VERBOSE )); then
			echo ">>    setting ${f} date to ${FYEAR}:${FMONTH}:${FDAY} ${FHOUR}:${FMIN}:${FSEC}${TIMEZONE}"
		fi
		# Set all possible date and time fields
		#    Note: AllDates is a shortcut for 3 tag names: DateTimeOriginal, CreateDate and ModifyDate	
		exiftool -m -q "-AllDates=${FYEAR}:${FMONTH}:${FDAY} ${FHOUR}:${FMIN}:${FSEC}${TIMEZONE}" \
			"-DateCreated=${FYEAR}:${FMONTH}${FDAY}" "-TimeCreated=${FHOUR}:${FMIN}:${FSEC}${TIMEZONE}" \
			"-DigitalCreationTime=${FHOUR}:${FMIN}:${FSEC}${TIMEZONE}" \
			"-DigitalCreationDate=${FYEAR}:${FMONTH}${FDAY}" \
			"-CreationDate=${FYEAR}:${FMONTH}${FDAY} ${FHOUR}:${FMIN}:${FSEC}${TIMEZONE}" \
			"${f}"
		(( ORIGINALS = 1 ))

		# Check if GPS should be modified
		if [[ ${CITY} && `exiftool -ee -GPSPosition "$f"` == "" ]]; then
			echo ">>>> Updating GPS location for ${f} to ${GPSLatitude}, ${GPSLongitude}"
			# Note that the "Ref" fields must be set for Google to recognize the GPS tags
			exiftool -m -q "-GPSLatitude=${GPSLatitude}" "-GPSLongitude=${GPSLongitude}" "-GPSLatitudeRef=North" "-GPSLongitudeRef=West" "${f}"
		fi

		(( SEC = SEC + $INCREMENT ))
		if (( SEC > 59 )); then
			(( SEC = 0 ))
			(( MIN = MIN + 1 ))
			if (( MIN > 59 )); then
				(( MIN = 0 ))
				(( HOUR = HOUR + 1 ))
				if (( HOUR > 23 )); then
					(( HOUR = 0 ))
					(( DAY = DAY + 1 ))
					# Sloppy month increment, since all we care about is order
					if (( DAY > 27 )); then
						(( MONTH = MONTH + 1 ))
						(( DAY = 1 )) 
						if (( MONTH > 12 )); then
							(( YEAR = YEAR + 1 ))
							(( MONTH = 1 ))
						fi
					fi
				fi
			fi
		fi
    done

	if (( ORIGINALS )); then
		echo "Deleting originals in ${FULLDIR}"
		rm *_original
	fi

	popd >/dev/null

done


    
