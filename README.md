Now that you have used Super Apple Photos Export to export your Apple Photos album, you may want to upload them to Google Photos.  However: Google Photos organizes photos in albums by date (there is no option for organizing photos alphabetically by file name) so many of your beautiful albums will become disorganized!  

mass-change-photo-datas.sh is a command line script that processes one or more folders at a time, and changes the timestamps so that the increasing order of time matches the the alphabetic order of file names.  

After processing, upload the contents of your folder to Google Photos, recreate the album from the "last uploaded" set of photos, and set the album sort to "date" rather than "upload order".  

The script handles the oddities of how Google Photo determines timestamps and timezones, which is not at all what you might expect.  

Requires the free program exiftool.

